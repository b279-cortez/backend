// console.log("hello world!");

// 1.
fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(title => {
	let returnTitle = title.map(dataToMap => {
		return {
			title: dataToMap.title
		};
	});
	console.log(returnTitle);
});


// 2.
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(json => console.log(json));


// 3.
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(json => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));


// 4.
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		completed: false,
		id: 1,
		title: "Created To Do List Item",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));


// 5.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		tite: "Updated To Do List Item",
		description: "To update the my to do list with a different data structure.",
		status: "Pending",
		dateCompleted: "Pending",
		userId: 1
	})
})
.then(res => res.json())
.then(json => console.log(json));

// 6.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {"Content-Type" : "application/json"},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "07/09/21"
	})
})
.then(res => res.json())
.then(json => console.log(json));

// 7.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
});