// 1.

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: null, fruitsOnSale: {$sum: 1}}},
  {$project: {_id: 0}}
]);

// 2.

db.fruits.aggregate([
	{$match: {stock: {$gte: 20}}},
	{$group: {_id: "$supplier_id", enoughStock: {$sum: 1}}},
	{$project: {_id: 0}}
]);

// 3.

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}},
]);

// 4.

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
	{$sort: {total: -1}}
]);

// 5.

db.fruits.aggregate([
	{$group: {_id: "$supplier_id", max_price: {$min: "$price"}}}
]);