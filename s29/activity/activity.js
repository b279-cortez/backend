// 1.

db.users.find(
	{
		$or: [{firstName: {$regex: "S"}}, {lastName: {$regex: "D"}}]
	},
	{
		firstName: 1,
		lastName: 1,
		_id: 0
	}
);

// 2.

db.users.find(
	{
		$and: [{department: "HR"}, {age: {$gte: 70}}]
	}
);

// 3.

db.users.find(
	{
		$and: [{firstName: {$regex: "e", $options: "i"}}, {age: {$lte: 30}}]
	}
);