const express = require("express");
const mongoose = require("mongoose");


const app = express();
const port = 3001;

// [Section] MongoDB connection

	// Connect to the database by passing in your connection string, remember to replace the password and database names with actual values
	// Due to updates in Mongo DB drivers that allow connection to it, the default connection string is being flagged as an error
	// By default a warning will be displayed in the terminal when the application is run, but this will not prevent Mongoose from being used in the application

mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.3xonz25.mongodb.net/B279_to-do?retryWrites=true&w=majority", 
		{
			useNewUrlParser : true,
			useUnifiedTopology : true
		}
	);

let db = mongoose.connection;

// Checks if there is an error in connecting
db.on("error", console.error.bind(console, "connection error"));
// Confirms if connection i successful
db.once("open", () => console.log("We're connected to the cloud database"));



// [SECTION] Mongoose Schemas
// Schemas determine the structure of the documents to be written in the database

const taskSchema = new mongoose.Schema({
	name : String,
	// There is a field called "status" that is a "string" and default value is "pending"
	status : {
		type : String,
		default : "pending"
	}
});

// User Schema
const userSchema = new mongoose.Schema({
	username : String,
	password: String
});


// [SECTION] Models

const Task = mongoose.model("Task", taskSchema);

// User Model
const User = mongoose.model("User", userSchema);


// [SECTION] Creation of todo lists routes
// Setup for allowing the server to handle data from request
// Allows your app to read json data
// MIDDLEWARES

app.use(express.json());
// urlencoded -> allows our app to read data from forms
app.use(express.urlencoded({extended:true}));

// CREATING A NEW TASK
// Business Logic

app.post("/tasks", (req, res) => {
	Task.findOne({name : req.body.name}).then((result, err) => {
		if (result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		}else {
			// Create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			})

			newTask.save().then((savedTask, saveErr) => {
				if(saveErr){
					// Will print any errors found in the console
					return console.error(saveErr);
				}else{
					// Return status code 201 for created
					return res.status(201).send("New task created");
				}
			})
		}
	})
});

// GETTING ALL TASKS
// Business Logic
	/*
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are found, send a success status back to the client/Postman and return an array of documents
	*/

app.get("/tasks", (req, res) => {
	Task.find({}).then((result, err) => {
		// If error occured
		if (err) {
			// Will print error message
			return console.log(err);
		}else {
			return res.status(200).json({
				data : result
			})
		}
	})
});

// CREATING USER
app.post("/signup", (req, res) => {
    User.findOne({
        username: req.body.username
    }).then((result, err) => {
        if (result != null && result.username == req.body.username) 
        {
            return res.send("Duplicate user found");
        } else if (!req.body.username || !req.body.password) {
            return res.send("Both username and password must be provided.");

        } else {
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            });
            newUser.save().then((savedUser, savedErr) => {
                if (savedErr) {
                    return console.error(savedErr);
                } else {
                    return res.status(200).send("New user registered");
                }
            });

        }
    });
});

// GET ALL USER
app.get("/signup", (req, res) => {
	User.find({}).then((result, err) => {
		// If error occured
		if (err) {
			// Will print error message
			return console.log(err);
		}else {
			return res.status(200).json({
				data : result
			})
		}
	})
});


if (require.main === module) {
	app.listen(port, () => console.log(`Server running at port ${port}`));
}