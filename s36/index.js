// Setup the dependencies/packages
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute.js")

// Server setup/middlewares
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Add task route
// Allows all the task route created in the taskRoute.js file to use /tasks route
app.use("/tasks", taskRoute);

// DB Connection
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.3xonz25.mongodb.net/B279_to-do?retryWrites=true&w=majority", 
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});




// Server listening
if (require.main === module) {
	app.listen(port, () => console.log(`Server runnning at port ${port}`));
}

module.exports = app;