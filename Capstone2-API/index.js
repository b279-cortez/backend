const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// 
const userRoutes = require("./routes/user.js");
const productRoutes = require("./routes/product.js");

// server
const app = express();

// MongoDB Connection using SRV Link
mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.3xonz25.mongodb.net/Capstone2_API?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// MongoDB Connection Validation
mongoose.connection.once("open", () => console.log("Now connected to MongoDB."));

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended : true}));

//
app.use("/users", userRoutes);
app.use("/products", productRoutes);


//Port Listening
if(require.main === module){
	app.listen(process.env.PORT || 4000, () => console.log(`API is now online on port ${process.env.PORT || 4000}.`));
}

module.exports = app;