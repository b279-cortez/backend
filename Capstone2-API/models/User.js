const mongoose = require("mongoose");


const userSchema = new mongoose.Schema({

	firstName : {
		type : String,
		required : [true, "first name required!"]
	},
	lastName : {
		type : String,
		required : [true, "last name required!"]
	},
	email : {
		type : String,
		required : [true, "email required!"]
	},
	password : {
		type : String,
		required: [true, "password required!"]
	},
	mobileNumber : {
		type : String,
		required: [true, "mobile number required!"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	orderedProducts : [
		{
			product : [
				{
					productId : {
						type : String,
						required : [true, "product id required!"]
					},
					productName : {
						type : String,
						required : [true, "product name required!"]
					},
					quantity : {
						type : Number,
						required : [true, "product quanityt required!"]
					}
				}
			],
			totalAmount : {
				type : Number,
				required : [true, "total amount required!"]
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			}
		}
	]

});


module.exports = mongoose.model("User", userSchema);