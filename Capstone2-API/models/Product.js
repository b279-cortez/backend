const mongoose = require("mongoose");


const productSchema = new mongoose.Schema ({
	name : {
		type : String,
		required : [true, "product name required"]
	},
	description : {
		type : String,
		required : [true, "product description required!"]
	},
	price : {
		type : Number,
		required : [true, "product price required!"]
	},
	image: {
		type : String,
		required : [true, "product image required!"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	userOrders : [
		{
			userId : {
				type : String,
				required : [true, "user id required!"]
			}
		}
	]
});


module.exports = mongoose.model("Product", productSchema);