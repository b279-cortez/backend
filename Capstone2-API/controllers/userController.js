const User = require("../models/User.js");
const Product = require("../models/Product.js");

const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// Function for checking if user email already exists.
module.exports.checkEmail = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {

		if (result.length > 0) {
			return true;
		} else {
			return false;
		}
	});
}


// Function for registering a user
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
				firstName : reqBody.firstName,
				lastName : reqBody.lastName,
				email : reqBody.email,
				mobileNumber : reqBody.mobileNumber,
				password : bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {

		if (error) {

			return false;

		} else {

			return true;

		}
	});
};


// Function for logging in a user
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {

		if (result == null) {

			return false;

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {

				return {access : auth.createAccessToken(result)};

			} else {

				return false;

			};
		};
	});
};


// Function for creating order (admin not allowed)
module.exports.createOrder = async (data) => {

	if(data.isAdmin){
		return "ADMIN cannot do this action.";
	}else{

		let isUserUpdated = await User.findById(data.userId).then(user => {
			let isOrderedProductUpdated = Product.findById(data.product.productId).then(product => {
				user.orderedProducts.push({product : [
						{
							productId : data.product.productId,
							productName : product.name,
							quantity : data.product.quantity
						}
					],
					totalAmount : data.product.quantity * product.price

				});

				return user.save().then((user, error) => {
					if(error){
						return false;
					}else{
						return true;
					}
				});
			});

			if(isOrderedProductUpdated){
				return Promise.resolve(true);
			}else{
				return Promise.resolve(false);
			}
		})
		
		console.log(`productId ${data.product.productId}`);
		console.log(`userId ${data.userId}`)	

		let isProductUpdated = await Product.findById(data.product.productId).then(product => {

			product.userOrders.push({userId : data.userId});

			return product.save().then((product, error) => {
				if(error){
					return false;
				}else{
					return true;
				}
			})
		})


		if(isUserUpdated && isProductUpdated){
			return true;
		}else{
			return false;
		}

	}
	
}


// Function for creating order (admin not allowed) another way/method
/*module.exports.createOrder = async (data) => {

	if(data.isAdmin){
		return "ADMIN cannot do this action.";
	} else {
		let isUserUpdated = await User.findById(data.userId).then(user => {
			return Product.findById(data.product.productId).then(result => {

				console.log(result.name);

				let newOrder = {
					product : [
						{
							productId : data.product.productId,
							productName : result.name,
							quantity : data.product.quantity
						}
					],
					totalAmount : result.price * data.product.quantity
				}
				user.orderedProducts.push(newOrder);

				console.log(data.product.productId);

				return user.save().then((user, error) => {
					if (error) {
						return false;
					} else {
						return true;
					}
				});
			});
		});

		let isProductUpdated = await Product.findById(data.product.productId).then(product => {

			product.userOrders.push({userId : data.userId})

			return product.save().then((product, error) => {
				if (error) {
					return false;
				} else {
					return true;
				}
			});
		});

		if(isUserUpdated && isProductUpdated){
			return "Thank you for your order!";
		}else {
			return "Something went wrong with your order!";
		}
	}
}*/


// Function for retrieving user details
module.exports.userDetails = (userData) => {

	return User.findById(userData).then(result => {

		result.password = "";

		return result;

	});

}


// STRETCH GOAL 1
// Function for setting user as admin (admin only)
module.exports.userToAdmin = (data) => {

	let updateUserAdminStatus = {
		isAdmin : true
	}

	if (data.isAdmin) {

		return User.findByIdAndUpdate(data.userId, updateUserAdminStatus).then((user, error) => {

			if (error) {

				return false;

			} else {

				return "You successfully set this user as admin!";

			}

		});

	}

	let message = Promise.resolve("You do not have the access rights to do this action!");

	return message.then(value => {

		return message;

	});

}