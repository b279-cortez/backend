const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth.js");


// Route for creating a new product (admin only)
router.post("/createNewProduct", auth.verify, (req, res) => {

	const data = {
		product : req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.createProduct(data).then(resultFromProductController => res.send(resultFromProductController));
});


// Route for retreieving all products (admin only)
router.get("/allProducts", (req, res) => {

	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	productController.retrieveProduct(isAdmin).then(resultFromProductController => res.send(resultFromProductController));

});


// Rroute for retrieving all "Active" products
router.get("/activeArtworks", (req, res) => {

	productController.activeArtworks().then(resultFromProductController => res.send(resultFromProductController));

});


// Route for retrieving a single product
router.get("/:productId", (req, res) => {

	productController.getSpecificProduct(req.params).then(resultFromProductController => res.send(resultFromProductController));

});


// Route for updating product information (admin only)
router.put("/:productId", auth.verify, (req, res) => {

	const data = {

		product : req.body,
		productId : req.params.productId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin

	}

	productController.updateProduct(data).then(resultFromProductController => res.send(resultFromProductController));

});


// Route for archiving product (admin only)
router.put("/:productId/archiveProduct", (req, res) => {

	const data = {
		productId : req.params.productId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(data).then(resultFromProductController => res.send(resultFromProductController));

});

// Route for unarchiving product (admin only)
router.put("/:productId/unarchiveProduct", (req, res) => {

	const data = {
		productId : req.params.productId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.unarchiveProduct(data).then(resultFromProductController => res.send(resultFromProductController));

});



module.exports = router;