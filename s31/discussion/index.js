// Use "require" directive to load Node.js modules
// A "http module" lets Node.js transfer data using Hyper Text Transfer Protocol (or HTTP)
// Clients (browser) and servers (node JS/express JS applications) communicate by exchanging individual messages

let http = require("http");

// Using this module's createServer() method, we can create an HTTP server that listens to requests on specified port.
// A port is a virtual point where network connection start and end.
// Each port is a associated with a specific process or service
// The server will be assigned to port 4000 using .listen()
http.createServer(function (request, response) {
	// Use writeHead() method to:
	// Set a status code for the message - 200 means okay
	// Set the Content-Type of the response as a plain text
	response.writeHead(200, {"Content-Type" : "text/plain"});
	response.end("Hello World!");
}).listen(4000);

console.log("Server running at localhost:4000");